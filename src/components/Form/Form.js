import React, { useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import styles from './Form.module.scss';
import { CONSTANTS } from '../../global/i18n/constants';
import utils from '../../global/utils';
import ListTable from '../ListTable/ListTable';

const Form = ({ validateNumbers, updateInput, setIsBackShow, setCardData }) => {
  const { register, handleSubmit,reset, formState: { errors } } = useForm();
  const [listItems, setListItems] = useState([]);
  const [showList, setShowList] = useState(false);
  const { handleChange } = validateNumbers();

  useEffect(() => {
    if (listItems.length < 1) {
      let list = utils.getList();
      setListItems(list);
    }
  })

  const onSubmit = data => {
    utils.save(data);
    setShowList(true)
  }

  const emptyFields = () => {
    setCardData({
      carNames: '',
      ccv: '',
      number: ['', '', '', ''],
      month: '',
      year: ''
    })
    reset();
    setShowList(false);
  }


  return (
    !showList ?
      <div className={styles.form}>
        <div className={styles.formContainer}>
          <div className={styles.formItems} >
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className={styles.fields}>
                <label>{CONSTANTS.texts.creditCardNumber}</label>
                <div className={styles.credit_card_numbers}>
                  <input type="number" id="ssn0" {...register("number[0]", ...CONSTANTS.validCardNumber)} onInput={handleChange} />
                  <input type="number" id="ssn1" {...register("number[1]", ...CONSTANTS.validCardNumber)} onInput={handleChange} />
                  <input type="number" id="ssn2" {...register("number[2]", ...CONSTANTS.validCardNumber)} onInput={handleChange} />
                  <input type="number" id="ssn3" {...register("number[3]", ...CONSTANTS.validCardNumber)} onInput={handleChange} />
                </div>
              </div>
              <div className={styles.fields}>
                <label>{CONSTANTS.texts.cardHolderName}</label>
                <div className={styles.credit_card_names}>
                  <input id='carNames' {...register("carNames", { required: true })} onInput={(e) => updateInput('carNames', e)} />
                </div>
              </div>
              <div className={[styles.fields, styles.secondFields].join(' ')}>
                <div>
                  <label>{CONSTANTS.texts.validThru}</label>
                  <div className={styles.credit_card_thru}>
                    <select {...register("month", { required: true })} onChange={(e) => updateInput('month', e)}>
                      {CONSTANTS.months.map((option, index) => (
                        <option key={index} value={option.value}>{option.text}</option>
                      ))}
                    </select>
                    <select {...register("year", { required: true })} onChange={(e) => updateInput('year', e)}>
                      {CONSTANTS.years.map((option, index) => (
                        <option key={index} value={option.value}>{option.text}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className={styles.ccv_container}>
                  <label>{CONSTANTS.texts.ccv}</label>
                  <input type="number" {...register("ccv", { required: true })} maxLength={3}
                    onFocus={() => setIsBackShow(true)}
                    onBlur={() => setIsBackShow(false)}
                    onInput={e => {
                      if (e.target.value.length >= 3) {
                        e.target.value = e.target.value.slice(0, 3)
                      }
                      updateInput('ccv', e)
                    }} />
                </div>
              </div>
              <div className={styles.submit_container}>
                <input className={styles.submit} type="submit" value={CONSTANTS.texts.submit} />
              </div>
            </form>
          </div>
        </div>
      </div>
      : <ListTable setShowList={emptyFields} />
  )
}

export default Form;
