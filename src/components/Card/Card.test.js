import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Footer from './Card';

describe('<Card />', () => {
  test('it should mount', () => {
    render(<Footer />);

    const card = screen.getByTestId('Card');

    expect(card).toBeInTheDocument();
  });
});