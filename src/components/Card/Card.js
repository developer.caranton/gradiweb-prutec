import React from 'react';
import styles from './Card.module.scss';
import { CONSTANTS } from '../../global/i18n/constants';


const Card = ({ cardData, isBackShow }) => (
  <div className={styles.card}>
    <div className={[styles.card__front, styles.card__part, !isBackShow ? styles.back_card: styles.front_card].join(' ')} >
      <p className={[styles.card__front_namebank, styles.card__namebank].join(' ')}>{CONSTANTS.cardTexts.bankName}</p>
      <div className={styles.card_icons}>
        <img className={styles.card_ship} src={CONSTANTS.cardTexts.ship} alt="ship"/>
        <img className={styles.card__square} src={CONSTANTS.cardTexts.square} alt="world"/>
      </div>
      <p className={styles.card_numer}>{`${cardData.number[0]} ${cardData.number[1]} ${cardData.number[2]} ${cardData.number[3]}`}</p>
      <div className={styles.card__space_50}>
        <span className={styles.card__label}>{CONSTANTS.cardTexts.cardHolder}</span>
        <p className={styles.card__info}>{cardData.carNames}</p>
      </div>
      <div className={styles.card__space_30}>
        <span className={styles.card__label}>{CONSTANTS.cardTexts.validThru}</span>
        <p className={styles.card__info}>{cardData.month}{cardData.month && '/'}{cardData.year}</p>
      </div>
      <div className={styles.card__space_20}>
        <img className={styles.card__visa_logo} src={CONSTANTS.cardTexts.visa} alt="visa"/>
      </div>
    </div>

    <div className={[styles.card__back, styles.card__part, isBackShow ? styles.back_card: styles.front_card].join(' ')} >
      <div className={styles.card__black_line}></div>
      <div className={styles.card__back_content}>
        <div className={styles.card__secret}>
          <p className={styles.card__secret__last}>{cardData.ccv}</p>
        </div>
      </div>
    </div>

  </div>
);

export default Card;
