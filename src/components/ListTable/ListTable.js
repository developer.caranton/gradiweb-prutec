import React, { useEffect, useState } from 'react';
import { CONSTANTS } from '../../global/i18n/constants';
import styles from './ListTable.module.scss';
import utils from '../../global/utils';

const ListTable = ({ setShowList }) => {
  const [listItems, setListItems] = useState([]);
  
  useEffect(() => {
    if (listItems.length < 1) {
      let list = utils.getList();
      setListItems(list);
    }
  })

  return (
    <div className={styles.table}>
      <div className={styles.tableContainer}>
        <div className={styles.tableItems} >
          <table className={styles.tableValues}>
            <thead>
              <tr>
                <td>{CONSTANTS.texts.creditCardNumber}</td>
                <td>{CONSTANTS.texts.cardHolderName}</td>
                <td>{CONSTANTS.texts.validThru}</td>
              </tr>
            </thead>
            <tbody>
              {listItems.map((item, index) => (
                <tr key={index}>
                  <td>**** **** **** {utils.getLastNumber(item?.number[3])}</td>
                  <td>{item.carNames}</td>
                  <td>{item.month}/{item.year}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className={styles.back_container}>
            <div className={styles.back} onClick={() => setShowList(false)}>{CONSTANTS.texts.back}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ListTable;
