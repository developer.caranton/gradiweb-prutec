import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ListTable from './ListTable';

describe('<ListTable />', () => {
  test('it should mount', () => {
    render(<ListTable />);

    const listTable = screen.getByTestId('ListTable');

    expect(listTable).toBeInTheDocument();
  });
});