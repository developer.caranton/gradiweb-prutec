export const CONSTANTS = {
    cardTexts: {
        bankName: 'BANK NAME',
        cardHolder: 'CARD HOLDER',
        validThru: 'VALID THRU',
        ship: 'https://thumbs.dreamstime.com/b/credit-card-chip-closeup-116257075.jpg',
        square: 'https://t3.ftcdn.net/jpg/02/55/14/86/360_F_255148621_W7BqdOYTKeCXQJ0U8uzUT0k0rP1ELhci.jpg',
        visa: 'https://cdn-icons-png.flaticon.com/512/196/196578.png'
    },
    texts: {
        creditCardNumber: 'CREDIT CARD NUMBER',
        cardHolderName: 'CARD HOLDER NAME',
        validThru: 'VALID THRU',
        ccv: 'CCV',
        submit: 'SUBMIT DETAILS',
        back: 'GO BACK'
    },
    validCardNumber: [
        { required: true, minLength: 4, maxLength: 4 }
    ],
    months: [
        { value: '', text: '--Choose an option--' },
        { value: '01', text: 'Jan' },
        { value: '02', text: 'Feb' },
        { value: '03', text: 'Mar' },
        { value: '04', text: 'Apr' },
        { value: '05', text: 'May' },
        { value: '06', text: 'Jun' },
        { value: '07', text: 'Jul' },
        { value: '08', text: 'Aug' },
        { value: '09', text: 'Sep' },
        { value: '10', text: 'Oct' },
        { value: '11', text: 'Nov' },
        { value: '12', text: 'Dec' },
    ],
    years: [
        { value: '', text: '--Choose an option--' },
        { value: '23', text: '2023' },
        { value: '24', text: '2024' },
        { value: '25', text: '2025' },
        { value: '26', text: '2026' },
        { value: '27', text: '2027' },
        { value: '28', text: '2028' },
        { value: '29', text: '2029' },
        { value: '30', text: '2030' },
    ]
};
