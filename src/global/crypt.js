import CryptoJS from "crypto-js";

const sk = process.env.REACT_APP_SECRET_KEY;

const crypt = {
    encryptData: (item) => {
        const data = CryptoJS.AES.encrypt(
            JSON.stringify(item),
            sk
        ).toString();
        return data;
    },
    decryptData: (item) => {
        const bytes = CryptoJS.AES.decrypt(item, sk);
        const data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        return data;
    }
}
export default crypt;