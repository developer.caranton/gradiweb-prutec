const Storage = {
    temp: () => {
        return process.env.REACT_APP_SECRET_KEY;
    },
    get: (name) => {
        const getItem = sessionStorage.getItem(name);
        return JSON.parse(getItem);
    },
    set: (key, value) => {
        sessionStorage.setItem(key, JSON.stringify(value));
    },
    clean: () => {
        sessionStorage.clear();
    }
}

export default Storage;