import crypt from "./crypt";
import Storage from "./storage";

const utils = {
    save: (data) => {
        let saveItems = Storage.get('list');
        let items = [];
        if (saveItems != null) {
            items = saveItems;
        }
        data.number = data.number.map(item => crypt.encryptData(item));
        items.push(data);
        Storage.set('list', items);
    },
    getList: () => {
        let saveItems = Storage.get('list');
        return saveItems != null ? saveItems : [];
    },
    getLastNumber: (number) => crypt.decryptData(number)
}

export default utils;