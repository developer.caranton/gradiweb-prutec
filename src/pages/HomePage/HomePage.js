import React, { useState } from 'react';
import Card from '../../components/Card/Card';

import styles from './HomePage.module.css';
import Form from '../../components/Form/Form';

const HomePage = () => {
  const [cardData, setCardData] = useState({
    carNames: '',
    ccv: '',
    number: ['', '', '', ''],
    month: '',
    year: ''
  });
  const [isBackShow, setIsBackShow] = useState(false);

  const validateNumbers = () => {
    return {
      handleChange: e => {
        //e => e.target.value.length >= 3 ? e.target.value = e.target.value.slice(0, 3) : null
        const { value, name } = e.target;
        const [fieldIndex] = name.match(/(\d+)/);

        // Check if they hit the max character length
        if (value.length >= 4) {
          e.target.value = value.slice(0, 4);
          // Check if it's not the last input field
          if (parseInt(fieldIndex, 10) <= 3) {
            // Get the next input field
            const nextSibling = document.getElementById((fieldIndex < 3) ? `ssn${parseInt(fieldIndex, 10) + 1}` : 'carNames');
            // If found, focus the next field
            if (nextSibling !== null) {
              nextSibling.focus();
            }
          }
        }
        cardData.number[parseInt(fieldIndex, 10)] = e.target.value;
        setCardData({ ...cardData });
      }
    };
  };

  const updateInput = (item, e) => {
    switch (item) {
      case 'carNames': cardData.carNames = e.target.value; break;
      case 'ccv': cardData.ccv = e.target.value; break;
      case 'month': cardData.month = e.target.value; break;
      case 'year': cardData.year = e.target.value; break;
      default: break;
    }
    setCardData({ ...cardData })
  }

  return (
    <div
      className={styles.HomePage}
      data-testid="home">
      <div className={styles.container}>
        <Card
          cardData={cardData}
          isBackShow={isBackShow}
        />
        <Form
          cardData={cardData}
          setCardData={setCardData}
          validateNumbers={validateNumbers}
          updateInput={updateInput}
          setIsBackShow={setIsBackShow}
        />
      </div>
    </div>
  );
};

export default HomePage;
